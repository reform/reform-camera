# MNT Reform Camera

![MNT Reform Camera in Action](images/reform-camera-v2-action1.jpg)

This is the open hardware USB 3.0 camera add-on for the [MNT Reform open hardware laptop](https://www.crowdsupply.com/mnt/reform). The goal is to provide an open solution for HD video telephony. The camera can also be used standalone or incorporated into other devices. The MNT Reform Camera electronics were designed by F. Schneider for MNT Research GmbH, Berlin.

## Version 2 and Laptop Integration

As of August 2022, we have successfully brought up the camera in the lab and fixed minor bugs in the firmware. After initial feedback, we decided to redesign the case to be slimmer and make it rotate on 2 axes. We also gave up on magnet mounting the camera and decided to go with a more universal clip and screw solution. Moreover, we decided to split the case into three parts, so that the two front halves can grab the sensors' lens mount. This allows for easy (dis)assembly without glue and manual adjustment of the fixed focus by rotating the lens in its thread. Greta Melnik designed a new case and mount from scratch in FreeCAD, and we 3D printed it in our lab before going into production with an aluminum version.

![MNT Reform Camera Case V2 Exploded](case-v2/case-v2-explosion.png)

We successfully tested the camera on MNT Reform and on Linux and Mac computers, as well as an Android phone.

![MNT Reform Camera in Action](images/reform-camera-v2-action2.jpg)
![MNT Reform Camera in Action](images/reform-camera-v2-action3.jpg)

## Electronics

The electronics design is made with version 6 of [KiCAD, a free and open source EDA program](https://www.kicad.org/).

## Specs

- MIPI-CSI to USB 3.0 bridge: Cypress EZ-USB CX3
- Image sensor: Omnivision OV5640 (modular, on 24-pin FPC connector)
- 4Mb SPI Flash for firmware (open source)
- Red activity LED
- Connector: USB-C with automatic plug reversing
- JTAG and UART debug connectors
- Schematics: [PDF](mnt-reform-camera-schematics-d1.pdf)

![MNT Reform Camera Schematics](images/reform-camera-schematics.png)

## License

All hardware sources are licensed under the [CERN Open Hardware Licence Version 2 - Strongly Reciprocal](https://ohwr.org/project/cernohl/wikis/uploads/002d0b7d5066e6b3829168730237bddb/cern_ohl_s_v2.txt).

## Funding

![NLNet logo](images/nlnet-320x120.png)

This project [is made possible through funding by NLNet](https://nlnet.nl/project/MNT-Reform/). Thanks!

![MNT Reform Camera Render](images/reform-camera-splash2.png)
